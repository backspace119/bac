// Bonk'd Audio Capture SDL Test program file

/*
 *  BAC Test application, simple audio visualizer using the BAC library
 *  Copyright (C) 2022  Bryan Sharpe
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BAC_BACSDL_H
#define BAC_BACSDL_H
#include <SDL2/SDL.h>
#include <stdint.h>
#include <fftw3.h>
#include "BAC.h"

typedef union
{
    uint32_t color;
    struct
    {
        uint8_t a, b, g, r;
    };
} BAC_Color_t;

#define BAC_SDL_FLAGS_DOUBLE_BUFFER 0x01 /* use this flag to double buffer the BAC_SDL sample buffer, on render, we   \
                                          * will copy data out of the main buffer and read from there for rendering   \
                                          * this can help prevent tearing between audio frames if render is slow and  \
                                          * we missed a frame, or if the BAC_SDL buffers have been passed directly to \
                                          * BAC it allows us to render without worrying that BAC might barge in and   \
                                          * dump garbage on us                                                        \
                                          */
typedef uint32_t BAC_SDLFlags_t;

/**
 * @param sampleRenderCount amount of samples to render, this stacks the sample size, larger values get you more data on the screen but higher compute costs good value is 10ish
 * @param w starting width of the program
 * @param h starting height of the program
 * @param maxFreqSpectDraw maximum frequency you want to display on the spectrograph, 20k is top of human hearing
 */
typedef struct
{
    int32_t w, h, nw, nh;
    uint32_t sampleRenderCount;
    uint32_t fftSampleCount;
    uint32_t maxFreqSpectDraw;
    BAC_Color_t background;
    BAC_Color_t freqGraph;
    BAC_Color_t powerSquare;
    BAC_SDLFlags_t flags;
} BAC_SDLParams_t;
/**
 * BAC_SDL_t context structure, do not modify anything here on your own
 */
typedef struct
{
    SDL_Window *window;
    SDL_Renderer *renderer;
    float prevTotalValue;
    fftwf_complex **in, **out;
    fftwf_plan *plan;
    float *wsampleBuffer;
    float *rsampleBuffer;
    float *windowBuffer;
    float *windowFuncBuf;
    BAC_SDLParams_t params;
    BACParams_t *sparams;
    uint8_t draw_cross;
} BAC_SDL_t;

/**
 * Initialize SDL and the renderer with values
 * @param sdl BAC_SDL_t context that you'll pass to all other functions
 * @param params parameters to initialize BAC_SDL with
 * @param sparams parameters you initialized BAC with
 */
void bacsdl_init(BAC_SDL_t *sdl, BAC_SDLParams_t params, BACParams_t *sparams);
/**
 * you should prefer getting the current sample pointer rather than using this function, it is provided for ease of use,
 * but will have an additional memcpy you can avoid by getting the pointer and filling it directly
 * @param sdl BAC_SDL_t context
 * @param sample sample to add, should be sizeof(float) * sampleSize large
 */
void bacsdl_addSample(BAC_SDL_t *sdl, const float *sample);
/**
 * get pointers into the BAC_SDL sample buffer, these all point into the same allocation, a single, large buffer is used
 * to help with memory locality
 * @param sdl BAC_SDL_t context
 * @param sample pointer to pointers that should be sizeof(float*) * channels large
 */
void bacsdl_getSamplePointers(BAC_SDL_t *sdl, float **sample);
/**
 * shifts samples in the internal buffer to prepare for writing a new sample
 * @param sdl BAC_SDL_t context
 */
void bacsdl_shiftSamples(BAC_SDL_t *sdl);
/**
 * render everything, the order of drawing is spectograph first, then freq graph, then power squares, background is
 * achieved by setting the clear color, it is not drawn in this function but the value is set
 * @param sdl BAC_SDL_t context
 */
void bacsdl_render(BAC_SDL_t *sdl);
/**
 * clear the screen, you should call this before calling to render, otherwise the framebuffer will be in an indeterminate
 * state
 * @param sdl BAC_SDL_t context
 */
void bacsdl_clearScreen(BAC_SDL_t *sdl);
/**
 * present the render to the display, you should call this after rendering
 * @param sdl BAC_SDL_t context
 */
void bacsdl_present(BAC_SDL_t *sdl);
/**
 * cleanup, call this before you finish your program, when you want to free all resources related to the BAC_SDL_t context
 * @param sdl
 */
void bacsdl_destroy(BAC_SDL_t *sdl);
/**
 * poll for events, only 2 are listened to and only one will be pushed back to you, resize is handled internally, quit
 * will result in this function returning 1, you should call this regularly to keep the event buffer empty
 * @param sdl BAC_SDL_t context
 * @return 0 under normal circumstance, 1 when program quit has been requested
 */
uint8_t bacsdl_pollEvent(BAC_SDL_t *sdl);
#endif // BAC_BACSDL_H
