/*
 *  BAC Test application, simple audio visualizer using the BAC library
 *  Copyright (C) 2022  Bryan Sharpe
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/BACSDL.h"
#include <math.h>

static void fillWindowNuttall(BAC_SDL_t *sdl, float *windowFuncBuffer)
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        windowFuncBuffer[i] = (float)(0.355768 - ((0.487396 * cos((2 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount)) +
                                                  (0.144232 * (cos((4 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))) -
                                                  (0.0106411 * cos((6 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))));
    }
}

static void fillWindowBlackmanNuttall(BAC_SDL_t *sdl, float *windowFuncBuffer)
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        windowFuncBuffer[i] = (float)(0.35875 - ((0.48829 * cos((2 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount)) +
                                                 (0.14128 * (cos((4 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))) -
                                                 (0.01168 * cos((6 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))));
    }
}

static void fillWindowFlatTop(BAC_SDL_t *sdl, float *windowFuncBuffer)
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        windowFuncBuffer[i] = (float)((0.21557895 - ((0.41663158 * cos((2 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount)) + (0.277263158 * (cos((4 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))) - (0.083578947 * cos((6 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))) +
                                       (0.006947368 * cos((8 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount))));
    }
}

static void fillWindowHann(BAC_SDL_t *sdl, float *windowFuncBuffer)
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        windowFuncBuffer[i] = (float)((0.5 * (1 - (cos((2 * M_PI * i) / sdl->sparams->sampleSize * sdl->params.fftSampleCount)))));
    }
}

static void fillWindowRectangular(BAC_SDL_t *sdl, float *windowFuncBuffer) // best for single amplitude bar, all others give aliases in number of buckets
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        windowFuncBuffer[i] = 1.0f;
    }
}

static uint32_t checkFlag(BAC_SDLFlags_t flag, BAC_SDLParams_t params)
{
    return flag & params.flags;
}

void bacsdl_init(BAC_SDL_t *sdl, BAC_SDLParams_t params, BACParams_t *sparams)
{
    SDL_Init(SDL_INIT_VIDEO);
    sdl->params = params;
    sdl->params.nw = sdl->params.w;
    sdl->params.nh = sdl->params.h;
    if (sdl->params.fftSampleCount > sdl->params.sampleRenderCount)
        sdl->params.fftSampleCount = sdl->params.sampleRenderCount;
    sdl->sparams = sparams;
    sdl->window = SDL_CreateWindow("bac test", 0, 0, sdl->params.w, sdl->params.h, SDL_WINDOW_RESIZABLE);
    sdl->renderer = SDL_CreateRenderer(sdl->window, -1, SDL_RENDERER_ACCELERATED);

    sdl->wsampleBuffer = malloc(sizeof(float) * sdl->sparams->sampleSize * sdl->params.sampleRenderCount * sdl->sparams->channels);
    if (checkFlag(BAC_SDL_FLAGS_DOUBLE_BUFFER, sdl->params))
        sdl->rsampleBuffer = malloc(sizeof(float) * sdl->sparams->sampleSize * sdl->params.sampleRenderCount * sdl->sparams->channels);
    else
        sdl->rsampleBuffer = sdl->wsampleBuffer;
    sdl->windowFuncBuf = malloc(sizeof(float) * sdl->sparams->sampleSize * sdl->params.fftSampleCount);
    fillWindowRectangular(sdl, sdl->windowFuncBuf);
    sdl->in = malloc(sizeof(fftw_complex *) * sdl->sparams->channels);
    sdl->out = malloc(sizeof(fftw_complex *) * sdl->sparams->channels);
    sdl->plan = malloc(sizeof(fftw_plan *) * sdl->sparams->channels);
    sdl->windowBuffer = fftwf_malloc(sizeof(float) * sdl->sparams->sampleSize * sdl->params.fftSampleCount); // malloc(sizeof(float) * sdl->sparams->sampleSize * sdl->params.sampleRenderCount);
    for (int i = 0; i < sdl->sparams->channels; i++)
    {
        //        sdl->in[i] = fftwf_malloc(sizeof(fftwf_complex) * sdl->sparams->sampleSize * sdl->params.sampleRenderCount);
        sdl->out[i] = fftwf_malloc(sizeof(fftwf_complex) * sdl->sparams->sampleSize * sdl->params.fftSampleCount);
        sdl->plan[i] = fftwf_plan_dft_r2c_1d((int)(sdl->sparams->sampleSize * sdl->params.fftSampleCount), sdl->windowBuffer, sdl->out[i], FFTW_MEASURE);
    }

    SDL_SetRenderDrawBlendMode(sdl->renderer, SDL_BLENDMODE_BLEND);
    sdl->draw_cross = 1;
}

void bacsdl_shiftSamples(BAC_SDL_t *sdl)
{
    for (int i = 0; i < sdl->sparams->channels; i++)
    {
        memmove(&sdl->wsampleBuffer[(sdl->sparams->sampleSize * (sdl->params.sampleRenderCount * i))],
                &sdl->wsampleBuffer[(sdl->sparams->sampleSize * (sdl->params.sampleRenderCount * i)) + sdl->sparams->sampleSize],
                sizeof(float) * (sdl->sparams->sampleSize * (sdl->params.sampleRenderCount - 1)));
    }
}

void bacsdl_addSample(BAC_SDL_t *sdl, const float *sample)
{
    for (int i = 0; i < sdl->sparams->channels; i++)
    {
        bacsdl_shiftSamples(sdl);
        memcpy(&sdl->wsampleBuffer[(sdl->params.sampleRenderCount * sdl->sparams->sampleSize * (i + 1)) - sdl->sparams->sampleSize], &sample[sdl->sparams->sampleSize * i],
               sizeof(float) * sdl->sparams->sampleSize);
    }
}

void bacsdl_getSamplePointers(BAC_SDL_t *sdl, float **sample)
{

    for (int i = 0; i < sdl->sparams->channels; i++)
    {
        sample[i] = &sdl->wsampleBuffer[(sdl->params.sampleRenderCount * sdl->sparams->sampleSize * (i + 1)) -
                                        sdl->sparams->sampleSize];
    }
}

static void windowFunc(BAC_SDL_t *sdl, const float *in, float *out)
{
    for (int i = 0; i < sdl->sparams->sampleSize * sdl->params.fftSampleCount; i++)
    {
        out[i] = in[i] * sdl->windowFuncBuf[i];
    }
}

static float *fftStart(BAC_SDL_t *sdl, uint32_t channel)
{
    return &sdl->rsampleBuffer[(((sdl->sparams->sampleSize * sdl->params.sampleRenderCount) * channel)) - (sdl->params.fftSampleCount * sdl->sparams->sampleSize)];
}

void bacsdl_render(BAC_SDL_t *sdl)
{
    sdl->params.w = sdl->params.nw;
    sdl->params.h = sdl->params.nh;
    const float maxFreq = (float)sdl->params.maxFreqSpectDraw;
    const float width = ((float)sdl->sparams->sampleRate / (float)(sdl->sparams->sampleSize * sdl->params.fftSampleCount));
    float prevprev = sdl->prevTotalValue;
    sdl->prevTotalValue = 0;
    const uint32_t len = sdl->sparams->sampleSize * sdl->params.sampleRenderCount;
    const uint32_t fftLen = sdl->sparams->sampleSize * sdl->params.fftSampleCount;
    const float x_mod = fmaxf(((float)sdl->params.w / (maxFreq / width)), 1.0f);
    SDL_Point combinedPoints[sdl->params.w * sdl->sparams->channels];
    if (checkFlag(BAC_SDL_FLAGS_DOUBLE_BUFFER, sdl->params))
        memcpy(sdl->rsampleBuffer, sdl->wsampleBuffer, sizeof(float) * sdl->sparams->sampleSize * sdl->params.sampleRenderCount * sdl->sparams->channels);
    for (int c = 0; c < sdl->sparams->channels; c++)
    {
        uint32_t offset = c * sdl->sparams->sampleSize * sdl->params.sampleRenderCount;
        SDL_Rect frect;
        frect.x = (int)(sdl->params.w - (sdl->params.w / sdl->params.sampleRenderCount));
        frect.y = sdl->params.h / 2;
        frect.w = (int)(sdl->params.w / sdl->params.sampleRenderCount);
        float s = 0;
        windowFunc(sdl, fftStart(sdl, (c + 1)), sdl->windowBuffer);
        fftwf_execute(sdl->plan[c]);

        //        for(int i = 0; i < sdl->params.maxFreqSpectDraw/width; i++)
        //        {
        //            printf("%ihz: %f,%f\n",(int)(i * width),sdl->out[c][i][0],sdl->out[c][i][1]);
        //        }

        SDL_Point points[sdl->params.w];
        //        printf("separator ======================\n");
        int prevX = -1;
        for (uint32_t i = 0; i < len; i++)
        {
            float data = sdl->rsampleBuffer[i + offset];
            // freq graph
            int currentX = (int)((float)i * ((float)sdl->params.w / ((float)sdl->sparams->sampleSize * (float)sdl->params.sampleRenderCount)));
            //            printf("currentX: %i\n",currentX);
            if (currentX > prevX && currentX < sdl->params.w)
            {
                prevX = currentX;
                points[currentX].x = currentX;
                points[currentX].y = (int)(((float)sdl->params.h / 2.0f) + (data * ((float)sdl->params.h / 3.0f)));
                combinedPoints[(currentX * sdl->sparams->channels) + c] = points[currentX];
            }

            // background (absolute power)
            if (i > (sdl->sparams->sampleSize * sdl->params.sampleRenderCount) - (sdl->sparams->sampleSize * sdl->params.fftSampleCount))
                sdl->prevTotalValue += fabsf(data);
            // power squares
            s += data;
            // draw power squares
            if (i % sdl->sparams->sampleSize == (sdl->sparams->sampleSize - 1))
            {
                SDL_SetRenderDrawColor(sdl->renderer, sdl->params.powerSquare.r, sdl->params.powerSquare.g,
                                       sdl->params.powerSquare.b, sdl->params.powerSquare.a / sdl->sparams->channels);
                float roffset = (((float)(i + 1)) / (float)sdl->sparams->sampleSize) - 1;
                frect.x = sdl->params.w - (int)(((float)sdl->params.sampleRenderCount - (roffset)) * ((float)sdl->params.w / (float)(sdl->params.sampleRenderCount)));
                frect.h = ((int)((s / (float)((float)sdl->sparams->sampleSize / 5.0f)) * ((float)sdl->params.h / 2)));
                SDL_RenderFillRect(sdl->renderer, &frect);
                frect.h = -((int)((s / (float)((float)sdl->sparams->sampleSize / 5.0f)) * ((float)sdl->params.h / 2)));
                SDL_RenderFillRect(sdl->renderer, &frect);
                s = 0.0f;
            }
            // spectrograph
            const float freq = width * (float)i;
            if (freq > maxFreq || i > fftLen / 2)
                continue;
            SDL_Rect rect;
            rect.w = (int)x_mod;
            rect.x = (int)((float)i * x_mod);
            float re = sdl->out[c][i][0];
            float im = sdl->out[c][i][1];
            float mag = (re * re + im * im);
            float res = logf(mag);
            if (res < 0)
                res = 0;
            rect.h = (int)(res * (float)((float)sdl->params.h / 30.0f));
            rect.y = sdl->params.h - rect.h;
            int16_t nr = (int16_t)(255 * (((maxFreq - (freq * 2.0)) / (maxFreq))));
            uint8_t g = (uint8_t)(255 * (float)((freq >= maxFreq / 2) ? ((maxFreq - (freq)) / (maxFreq / 2)) : ((freq * 2) / (maxFreq))));
            int16_t nb = (int16_t)(255 * ((freq - (maxFreq / 2.0)) / (maxFreq / 2.0)));
            uint8_t b = (nb > 0) ? nb : 0;
            uint8_t r = (nr > 0) ? nr : 0;
            // draw spectrograph
            SDL_SetRenderDrawColor(sdl->renderer, r, g, b,
                                   (uint8_t)((255 * fmaxf(fminf(res / 2, 1.0f), 0.1f))) / sdl->sparams->channels);
            SDL_RenderFillRect(sdl->renderer, &rect);
        }
        // draw freq graph

        SDL_SetRenderDrawColor(sdl->renderer, sdl->params.freqGraph.r, sdl->params.freqGraph.g,
                               sdl->params.freqGraph.b, sdl->params.freqGraph.a / sdl->sparams->channels);
        SDL_RenderDrawLines(sdl->renderer, points, (int)sdl->params.w);
    }
    sdl->prevTotalValue /= (float)fftLen;
    sdl->prevTotalValue = fminf((float)sdl->prevTotalValue + prevprev / 2.0f, 1.0f);
    if (sdl->draw_cross)
    {
        SDL_SetRenderDrawColor(sdl->renderer, 0x7F, 0x7F,
                               0x7F, 0x7F);

        SDL_RenderDrawLines(sdl->renderer, combinedPoints, (int)(sdl->params.w * sdl->sparams->channels));
    }
}

void bacsdl_clearScreen(BAC_SDL_t *sdl)
{
    SDL_SetRenderDrawColor(sdl->renderer, (uint8_t)(sdl->prevTotalValue * (float)sdl->params.background.r),
                           (uint8_t)(sdl->prevTotalValue * (float)sdl->params.background.g),
                           (uint8_t)(sdl->prevTotalValue * (float)sdl->params.background.b),
                           (uint8_t)(sdl->prevTotalValue * (float)sdl->params.background.a));
    SDL_RenderClear(sdl->renderer);
}

void bacsdl_present(BAC_SDL_t *sdl)
{
    SDL_RenderPresent(sdl->renderer);
}
void bacsdl_destroy(BAC_SDL_t *sdl)
{
    for (int i = 0; i < sdl->sparams->channels; i++)
    {
        //        fftwf_free(sdl->in[i]);
        fftwf_free(sdl->out[i]);
        fftwf_destroy_plan(sdl->plan[i]);
    }
    free(sdl->in);
    free(sdl->out);
    free(sdl->plan);
    if (checkFlag(BAC_SDL_FLAGS_DOUBLE_BUFFER, sdl->params))
        free(sdl->rsampleBuffer);
    free(sdl->wsampleBuffer);
    free(sdl->windowFuncBuf);
    fftwf_free(sdl->windowBuffer);
}

uint8_t bacsdl_pollEvent(BAC_SDL_t *sdl)
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            return 1;
        }
        if (event.type == SDL_WINDOWEVENT)
        {
            if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
            {

                SDL_GetWindowSize(sdl->window, &sdl->params.nw, &sdl->params.nh);
                return 0;
            }
        }
        if (event.type == SDL_KEYDOWN)
        {
            sdl->draw_cross = !sdl->draw_cross;
            return 0;
        }
    }
    return 0;
}
