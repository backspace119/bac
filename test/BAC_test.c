/*
 *  BAC Test application, simple audio visualizer using the BAC library
 *  Copyright (C) 2022  Bryan Sharpe
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/BACSDL.h"
#include <time.h>
#include <unistd.h>
// #define BAC_DEBUG(...) printf(__VA_ARGS__)
#include "BAC.h"

typedef struct
{
    BAC_SDL_t *sdl;
    uint8_t signal;
} Poll_t;

void *pollInput(void *p)
{
    Poll_t *poll = p;
    while (!(poll->signal = bacsdl_pollEvent(poll->sdl)))
    {
        usleep(50);
    }
    return NULL;
}

uint8_t rec = 0;

void prepSDLBuffer(void *userdata, float **buffer, uint32_t channels, uint32_t sampleSize)
{
    BAC_SDL_t *sdl = (BAC_SDL_t *)userdata;
    bacsdl_shiftSamples(sdl);
}

//#define BAC_TEST_DEBUG
int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        if (strcmp("-r", argv[1]) == 0)
            rec = 1;
    }

    BACParams_t pwParams;
    const uint32_t sampleRenderAmount = 15;
    const size_t sampleSize = BAC_SAMPLE_SIZE_1K;
    const uint32_t sampleRate = BAC_SAMPLE_RATE_96K;
    pwParams.sampleRate = sampleRate;
    pwParams.channels = 2;
    pwParams.sampleSize = sampleSize;
#ifndef __MINGW64__
#ifndef BAC_NO_PW
    pwParams.interface = PIPEWIRE;
#else
    pwParams.interface = ALSA;
#endif
#else
    pwParams.interface = PORTAUDIO;
#endif
    pwParams.flags = BAC_FLAGS_CALL_BEFORE_WRITE | BAC_FLAGS_EXTERNAL_BUFFER;
    pwParams.bpcb = prepSDLBuffer;
    pwParams.userData = NULL;
    // only need one or the other of the following, I am including both since I regularly switch between ALSA and PW for testing
#ifndef BAC_NO_PW
    pwParams.pwInterface = BAC_DEFAULT_INTERFACE_PW;
#endif
    pwParams.alsaInterface = BAC_DEFAULT_INTERFACE_ALSA;

    BAC_SDL_t sdl;
    BAC_SDLParams_t params = {
        .sampleRenderCount = sampleRenderAmount,
        .fftSampleCount = 4,
        .w = 1920,
        .h = 1080,
        .maxFreqSpectDraw = 20000,
        .background.color = 0x7F1060FF,
        .powerSquare.color = 0xBA50307F,
        .freqGraph.color = 0xFFFFFFFF,
        .flags = BAC_SDL_FLAGS_DOUBLE_BUFFER

    };
    bacsdl_init(&sdl, params, &pwParams);
    pwParams.userData = &sdl;
    pwParams.externalBuffer = malloc(sizeof(float *) * pwParams.channels);
    bacsdl_getSamplePointers(&sdl, pwParams.externalBuffer);
    BAC_t *pctx;
    printf("initializing bac\n");
    int err;
    if ((err = bac_start(&pctx, &pwParams)))
    {
        printf("error %i", err);
        exit(err);
    }
    printf("Sample rate requested: %i sample rate given: %i\n", sampleRate, pwParams.sampleRate);

    bacsdl_clearScreen(&sdl);
    bacsdl_present(&sdl);

    float **sample = pctx->params.externalBuffer;
    Poll_t poll = {
        .sdl = &sdl,
        .signal = 0};
    pthread_t thread;
    pthread_create(&thread, NULL, pollInput, &poll);
    FILE **out;
    char *fileNames;
    if (rec)
    {
        out = malloc(sizeof(FILE *) * pwParams.channels);
        fileNames = malloc(pwParams.channels * 16);
        for (int i = 0; i < pwParams.channels; i++)
        {

            sprintf(&fileNames[i * 16], "test.%i", i);
            printf("opening file for writing: %s\n", &fileNames[i * 16]);
            out[i] = fopen(&fileNames[i * 16], "wb");
        }
    }
    struct timespec current, prev;

    while (!poll.signal)
    {
        pthread_mutex_lock(&pctx->readMutex);
        pthread_cond_wait(&pctx->readyCond, &pctx->readMutex);
        pthread_mutex_unlock(&pctx->readMutex);
        if (rec)
        {
            for (int i = 0; i < pwParams.channels; i++)
                fwrite(sample[i], sizeof(float), pwParams.sampleSize, out[i]);
        }
        clock_gettime(CLOCK_MONOTONIC, &prev);
        bacsdl_clearScreen(&sdl);
        bacsdl_render(&sdl);
        bacsdl_present(&sdl);
        clock_gettime(CLOCK_MONOTONIC, &current);
        int64_t elapsedTime = (current.tv_nsec - prev.tv_nsec) / 1000;
        elapsedTime += ((current.tv_sec - prev.tv_sec) * 1000000000);
#ifdef BAC_TEST_DEBUG
        printf("frame time us: %li\n", elapsedTime);
#endif
    }
    if (rec)
    {
        for (int i = 0; i < pwParams.channels; i++)
            fclose(out[i]);
        free(fileNames);
    }

    bac_stop(pctx);
    bacsdl_destroy(&sdl);
    free(pwParams.externalBuffer);
    return 0;
}