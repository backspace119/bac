
# README

# Version : 1.3.2
___

## Licensing
<p> __WARNING:__ This repository uses 2 licenses simultaneously. The library portion of this repo is licensed under the MIT license. <br>
The test program (the audio visualizer) is licensed under GPL V3. See the License files for details on exactly which files <br>
are coverd under what. I have also appended the correct license preamble to each file it applies to, as a general rule <br>
(read the licenses for the actual legal rule) you can use the library as you please, but if you make use of the test program <br>
in your software you MUST license your software under GPL V3 or a stricter GPL license, again, refer to the license files <br>
for the actual legal rule.</p>

___

## Building
### Library
#### Pre-Requisites

<p> Instructions are provided for Linux, if you're on windows, good luck. </p>
<p>
This library requires PortAudio, PipeWire* (or not, see notes below this) and ALSA to build. Likely you already have ALSA, but you may <br> 
not have PipeWire. Installing/configuring PipeWire is out of the scope of this Readme, refer to your distro's manual for how to do so. <br>
Fedora already includes both PipeWire and ALSA by default starting at (I believe) version 33. All testing was done on Fedora 36. <br>

PortAudio is available in Fedora repos and can be installed with dnf just the same as you'd install anything else. <br>
Check your distros repositories for it, or you can get the source and build it yourself.<br>
<br>
*There is a target to build without PipeWire, if you so choose, instructions below include these targets.<br>

</p>

##### Notes on Building:
<p>
In the following instructions I mention the `linux` and `linux-test` targets. If your distro does not have pipewire, and <br>
you do not want to deal with installing/configuring, you can alternatively use `linux-no-pw` or `linux-test-no-pw` respectively <br>
to build without pipewire, in these modes only the ALSA interface is available, and the shared object created by this is named <br>
`libbacnopw.so`
</p>

#### Make and Install
<p> clone this repository to wherever you want it, then from the root directory of the repository run:

    make linux

to build the library. The output `libbac.so` file will be in the `bin` directory created by this command. To install the <br>
library, run:

    make install

This will prompt for your sudo password, as it places the `libbac.so` in `/usr/local/lib`
</p>

### Test Program (audio visualizer)

#### WARNING: If you have epilepsy it's probably best to not use this application, it flashes colors pretty rapidly, you have been warned.

#### Pre-Requisites
<p> The test program additionally requires SDL2 and FFTW, specifically the 32 bit float version, for information on how to obtain <br>
this library and install it, refer to **[here](https://www.fftw.org/fftw3_doc/Installation-and-Customization.html)** you will need to use the --enable-shared and --enable-float flags when configuring FFTW3. <br>
For SDL2, you can likely install it from your distros package manager.<br>
For Fedora you can find relevant pacakges with

    dnf search SDL2

remember to install both the library AND the development headers, both are required.
</p>

#### Make and Run
<p> To make the test program run 
    
    make linux-test

the output binary file will be named `bac` and will be placed in the `bin` directory. Note this command runs the `linux` <br>
and `install` targets and so may ask for your sudo password in order to install the library. To run the audio visualizer run:

    cd bin
    ./bac <args>

There is only a single recognized argument as of version 1.0.0, which is `-r` to record the audio stream. <br>
***WARNING: The audio files produced by recording are MASSIVE as they are RAW AUDIO DATA!*** They will be output to <br>
`test.0` and `test.1` and can be played with `aplay` if you provide the correct rate and channel arguments. (by default <br>
these are 48khz and 2 respectively). Running without arguments does not record audio, and simply runs the audio visualizer. <br>
So put on some good tunes, sit back, and enjoy :)
</p>