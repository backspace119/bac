/*
 * Begin license text.
 *
 * Copyright 2022 Bryan Sharpe
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * End license text.
 */
#ifndef __MINGW64__
#include "include/BAC_ALSA.h"
#include <stdlib.h>
#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>

typedef struct
{
    BAC_t *ctx;
    snd_pcm_t *capture_handle;
    pthread_t sampleThread;
} BAC_ALSA_t;

static void *startThread(void *data)
{
    BAC_ALSA_t *ictx = data;
    float *buf;
    buf = malloc(sizeof(float *) * ictx->ctx->params.channels * ictx->ctx->params.sampleSize);
    float **bufs = malloc(sizeof(float *) * ictx->ctx->params.channels);
    for (int i = 0; i < ictx->ctx->params.channels; i++)
        bufs[i] = &buf[ictx->ctx->params.sampleSize * i];
    snd_pcm_start(ictx->capture_handle);
    const uint32_t sleepSize = ((uint32_t)(((float)ictx->ctx->params.sampleSize * (1.0f / (float)ictx->ctx->params.sampleRate)) * 1000000)) - 1000;
    while (ictx->ctx->run)
    {
        snd_pcm_sframes_t avail = snd_pcm_avail(ictx->capture_handle);

        BAC_DEBUG(ictx->ctx, "avail: %zu\n", avail);
        if (avail > ictx->ctx->params.sampleSize)
        {
            while (avail > ictx->ctx->params.sampleSize)
            {
                avail -= snd_pcm_readn(ictx->capture_handle, (void **)bufs, ictx->ctx->params.sampleSize);

                if (!pthread_mutex_trylock(&ictx->ctx->writeMutex))
                {
                    if (bac_checkFlag(BAC_FLAGS_CALL_BEFORE_WRITE, &ictx->ctx->params))
                    {
                        ictx->ctx->params.bpcb(ictx->ctx->params.userData, ictx->ctx->sample, ictx->ctx->params.channels, ictx->ctx->params.sampleSize);
                    }
                    for (int i = 0; i < ictx->ctx->params.channels; i++)
                        memcpy(ictx->ctx->sample[i], bufs[i], ictx->ctx->params.sampleSize * sizeof(float) * ictx->ctx->params.channels);
                    ictx->ctx->ready = 1;
                    pthread_mutex_unlock(&ictx->ctx->writeMutex);
                    if (bac_checkFlag(BAC_FLAGS_CALL_AFTER_WRITE, &ictx->ctx->params))
                    {
                        ictx->ctx->params.fcb(ictx->ctx->params.userData, ictx->ctx->sample, ictx->ctx->params.channels, ictx->ctx->params.sampleSize);
                    }
                    pthread_cond_signal(&ictx->ctx->readyCond);
                }
                else
                {
                    BAC_DEBUG(ictx->ctx, "dropped frame\n");
                }
            }
            usleep(sleepSize);
        }
    }
    //    for(int i = 0; i < ictx->ctx->params.channels;i++)
    //        free(buf[i]);
    free(buf);
    free(bufs);
    snd_pcm_close(ictx->capture_handle);
    BAC_t *ctx = ictx->ctx;
    free(ictx);
    ctx->done = 1;
    return NULL;
}

int bac_alsa_start(char *interface, BAC_t *ctx, BACParams_t *params)
{
    BAC_ALSA_t *ictx = malloc(sizeof(BAC_ALSA_t));
    int err;
    snd_pcm_t *capture_handle;
    snd_pcm_hw_params_t *hw_params;

    if ((err = snd_pcm_open(&capture_handle, interface, SND_PCM_STREAM_CAPTURE, 0)) < 0)
    {
        BAC_ERROR("cannot open audio device %s (%s)\n",
                  interface,
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0)
    {
        BAC_ERROR("cannot allocate hardware parameter structure (%s)\n",
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0)
    {
        BAC_ERROR("cannot initialize hardware parameter structure (%s)\n",
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_NONINTERLEAVED)) < 0)
    {
        BAC_ERROR("cannot set access type (%s)\n",
                  snd_strerror(err));
        return err;
    }

    //    if((err = snd_pcm_hw_params_set_buffer_size(capture_handle,hw_params,params->sampleSize * sizeof(float))))
    //    {
    //        fprintf (stderr, "cannot set buffer size (%s)\n",
    //                 snd_strerror (err));
    //        return err;
    //    }

    if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, params->channels)) < 0)
    {
        BAC_ERROR("cannot set channels (%s)\n",
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_FLOAT)) < 0)
    {
        BAC_ERROR("cannot set sample format (%s)\n",
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &params->sampleRate, 0)) < 0)
    {
        BAC_ERROR("cannot set sample rate (%s)\n",
                  snd_strerror(err));
        return err;
    }
    ctx->params = *params;

    if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2)) < 0)
    {
        BAC_ERROR("cannot set channel count (%s)\n",
                  snd_strerror(err));
        return err;
    }

    if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0)
    {
        BAC_ERROR("cannot set parameters (%s)\n",
                  snd_strerror(err));
        return err;
    }
    snd_pcm_hw_params_free(hw_params);

    if ((err = snd_pcm_prepare(capture_handle)) < 0)
    {
        BAC_ERROR("cannot prepare audio interface for use (%s)\n",
                  snd_strerror(err));
        return err;
    }
    ictx->ctx = ctx;
    ictx->capture_handle = capture_handle;
    pthread_create(&ictx->sampleThread, NULL, startThread, ictx);
    return 0;
}
#endif