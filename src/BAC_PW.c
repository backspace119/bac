/*
 * Begin license text.
 *
 * Copyright 2022 Bryan Sharpe
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * End license text.
 */
#ifndef __MINGW64__
#ifndef BAC_NO_PW
#include <spa/param/audio/format-utils.h>
#include <pipewire/pipewire.h>
#include "include/BAC_PW.h"
#include <pipewire/device.h>
#include <pipewire/port.h>
#include <spa/debug/dict.h>
#include <pthread.h>
#include <unistd.h>

typedef struct
{
    struct pw_main_loop *loop;
    struct pw_main_loop *recloop;
    struct pw_stream *stream;
    struct pw_stream *istream;
    const struct spa_pod *params[1];
    uint8_t cheekyBastard : 1;
    uint8_t quit : 1;
    BAC_t *ctx;
    struct pw_registry *registry;
    struct spa_hook registry_listener;
    struct spa_pod_builder b;
    uint8_t *buffer;
    struct pw_context *context;
    struct pw_core *core;
    size_t currentSampAmount;
    pthread_t sampleThread;
    char rate[32];
    char quantum[32];
    char rateQuantum[32];
} BAC_PW_ictx_t;

static void on_process(void *userdata)
{
    BAC_PW_ictx_t *ictx = userdata;
    if (ictx->quit)
        return;
    if (!ictx->istream)
        return;
    struct pw_buffer *b;
    struct spa_buffer *buf;

    if ((b = pw_stream_dequeue_buffer(ictx->istream)) == NULL)
    {
        pw_log_warn("out of buffers: %m");
        return;
    }

    buf = b->buffer;
    if (buf->datas[0].data == NULL)
        return;

    uint32_t offset = buf->datas[0].chunk->offset % buf->datas->maxsize;

    int64_t boundsCheck = (int64_t)((int64_t)ictx->ctx->params.sampleSize - (int64_t)ictx->currentSampAmount);
    BAC_DEBUG(ictx->ctx, "type: %i\n", buf->datas[0].type);
    BAC_DEBUG(ictx->ctx, "n_datas: %i\n", buf->n_datas);
    BAC_DEBUG(ictx->ctx, "offset: %i\n", offset);
    BAC_DEBUG(ictx->ctx, "size: %i\n", buf->datas[0].chunk->size);
    BAC_DEBUG(ictx->ctx, "max size: %i\n", buf->datas[0].maxsize);
    BAC_DEBUG(ictx->ctx, "bounds check: %zu\n", boundsCheck);

    if (!pthread_mutex_trylock(&ictx->ctx->writeMutex))
    {
        uint8_t notify = 0;
        if (!ictx->currentSampAmount && bac_checkFlag(BAC_FLAGS_CALL_BEFORE_WRITE, &ictx->ctx->params))
        {
            ictx->ctx->params.bpcb(ictx->ctx->params.userData, ictx->ctx->sample, ictx->ctx->params.channels, ictx->ctx->params.sampleSize);
        }
        for (int i = 0; i < ictx->ctx->params.channels; i++)
        {
            if (boundsCheck > (int)(((int)buf->datas[i].chunk->size) / sizeof(float)))
            {
                memcpy(&ictx->ctx->sample[i][ictx->currentSampAmount], buf->datas[i].data + offset, buf->datas[i].chunk->size);
            }
            else
            {
                memcpy(&ictx->ctx->sample[i][ictx->currentSampAmount], buf->datas[i].data + offset,
                       ((ictx->ctx->params.sampleSize - ictx->currentSampAmount) * sizeof(float)));
                notify = 1;
            }
        }
        pthread_mutex_unlock(&ictx->ctx->writeMutex);
        if (notify)
        {
            ictx->ctx->ready = 1;
            ictx->currentSampAmount = 0;
            if (bac_checkFlag(BAC_FLAGS_CALL_AFTER_WRITE, &ictx->ctx->params))
            {
                ictx->ctx->params.fcb(ictx->ctx->params.userData, ictx->ctx->sample, ictx->ctx->params.channels, ictx->ctx->params.sampleSize);
            }
            pthread_cond_broadcast(&ictx->ctx->readyCond);
        }
        else
        {
            ictx->ctx->ready = 0;
            ictx->currentSampAmount += (buf->datas[0].chunk->size / sizeof(float));
        }
    }
    else
    {
        BAC_DEBUG(ictx->ctx, "dropped frame\n");
    }
    pw_stream_queue_buffer(ictx->istream, b);
    if (!ictx->ctx->run)
    {
        pw_main_loop_quit(ictx->recloop);
        ictx->quit = 1;
    }
}

static const struct pw_stream_events stream_events = {
    PW_VERSION_STREAM_EVENTS,
    .process = on_process,
};

/*
 *  Before anyone sees this and wonders what in the name of fuck I am doing, allow me to explain, searching for information
 *  on PipeWire of which output device is default comes up mostly with people trying to fix their Arch installs, not information
 *  on actual development. On PipeWire's website, they have a couple of examples of getting data from streams, but never
 *  on how to find specific streams, i.e. the default output device. SO, knowing this, I decided the best way to go about
 *  it was to connect a fake output stream to "any ID", which should connect us to the default output device, then, we
 *  immediately disconnect that stream, and connect our real input stream to the same node, which should get us on the
 *  monitor sources of that output stream, also, the header says Object ID for stream connection, when it's actually
 *  node ID.
 */
static void registry_event_global(void *d, uint32_t id,
                                  uint32_t permissions, const char *type, uint32_t version,
                                  const struct spa_dict *props)
{
    BAC_PW_ictx_t *ictx = d;
    if (ictx->cheekyBastard)
        return;
    uint32_t node = pw_stream_get_node_id(ictx->stream);
    const char *linkOutputNode = spa_dict_lookup(props, PW_KEY_LINK_OUTPUT_NODE);
    if (linkOutputNode)
        if (strcmp(linkOutputNode, "(null)"))
        {
            if (atoi(linkOutputNode) == node)
            {
                const char *linkInputNode = spa_dict_lookup(props, PW_KEY_LINK_INPUT_NODE);
                uint32_t otherNode;
                if (linkInputNode)
                {
                    pw_stream_disconnect(ictx->stream);
                    pw_stream_destroy(ictx->stream);
                    otherNode = atoi(linkInputNode);
                }
                else
                {
                    return;
                }
                ictx->recloop = pw_main_loop_new(NULL);
                snprintf(ictx->rate, sizeof(ictx->rate), "%i", ictx->ctx->params.sampleRate);
                snprintf(ictx->quantum, sizeof(ictx->quantum), "%i", ictx->ctx->params.sampleSize);
                snprintf(ictx->rateQuantum, sizeof(ictx->rateQuantum), "%i/%i", ictx->ctx->params.sampleSize, ictx->ctx->params.sampleRate);
                ictx->istream = pw_stream_new_simple(
                    pw_main_loop_get_loop(ictx->recloop),
                    "bac-monitor",
                    pw_properties_new(
                        PW_KEY_MEDIA_TYPE, "Audio",
                        PW_KEY_MEDIA_CATEGORY, "Monitor",
                        PW_KEY_MEDIA_ROLE, "DSP",
                        PW_KEY_NODE_LATENCY, ictx->rateQuantum,
                        PW_KEY_NODE_MAX_LATENCY, ictx->rateQuantum,
                        PW_KEY_STREAM_LATENCY_MAX, ictx->quantum,
                        PW_KEY_STREAM_LATENCY_MIN, ictx->quantum,
                        //                            PW_KEY_STREAM_MONITOR,"true",
                        //#ifdef PW_KEY_NODE_FORCE_QUANTUM //to make this compatible with previous version of 0.3 pw, since they're retarded,
                        //                            PW_KEY_NODE_FORCE_QUANTUM,ictx->quantum,
                        //                            PW_KEY_NODE_FORCE_RATE,ictx->rate,
                        //#endif
                        PW_KEY_NODE_NAME, "bac-monitor",
                        NULL),
                    &stream_events,
                    ictx);

                BAC_DEBUG(ictx->ctx,"connecting node ID: %i\n", otherNode);
                pw_stream_connect(ictx->istream,
                                  PW_DIRECTION_INPUT,
                                  otherNode,
                                  PW_STREAM_FLAG_AUTOCONNECT |
                                      PW_STREAM_FLAG_MAP_BUFFERS |
                                      PW_STREAM_FLAG_RT_PROCESS,
                                  ictx->params, 1);
                ictx->cheekyBastard = 1;
                pw_main_loop_quit(ictx->loop);
            }
        }
}
static const struct pw_registry_events registry_events = {
    PW_VERSION_REGISTRY_EVENTS,
    .global = registry_event_global,
};

static void *runMainLoop(void *data)
{
    BAC_PW_ictx_t *ictx = data;
    pw_main_loop_run(ictx->loop);
    pw_proxy_destroy((struct pw_proxy *)ictx->registry);
    pw_core_disconnect(ictx->core);
    pw_context_destroy(ictx->context);
    pw_main_loop_destroy(ictx->loop);
    pw_main_loop_run(ictx->recloop);
    pw_stream_destroy(ictx->istream);
    pw_main_loop_destroy(ictx->recloop);
    free(ictx->buffer);
    pthread_mutex_destroy(&ictx->ctx->writeMutex);
    BAC_t *ctx = ictx->ctx;
    free(ictx);
    ctx->done = 1;
    return NULL;
}

int bac_pw_start(uint32_t interface, BAC_t *ctx, BACParams_t *params)
{
    BAC_PW_ictx_t *ictx = malloc(sizeof(BAC_PW_ictx_t));
    ictx->buffer = malloc(1024);
    ictx->b = SPA_POD_BUILDER_INIT(ictx->buffer, 1024);
    pw_init(0, NULL);
    ictx->ctx = ctx;
    ictx->loop = pw_main_loop_new(NULL);
    ictx->context = pw_context_new(pw_main_loop_get_loop(ictx->loop), NULL, 0);
    ictx->currentSampAmount = 0;
    ictx->core = pw_context_connect(ictx->context, NULL, 0);
    ictx->quit = 0;
    ictx->registry = pw_core_get_registry(ictx->core, PW_VERSION_REGISTRY, 0);

    spa_zero(ictx->registry_listener);
    pw_registry_add_listener(ictx->registry, &ictx->registry_listener,
                             &registry_events, ictx);
    ictx->cheekyBastard = 0;
    ictx->stream = pw_stream_new_simple(
        pw_main_loop_get_loop(ictx->loop),
        "bac-test",
        pw_properties_new(
            PW_KEY_MEDIA_TYPE, "Audio",
            PW_KEY_MEDIA_CATEGORY, "Monitor",
            PW_KEY_MEDIA_ROLE, "DSP",
            NULL),
        &stream_events,
        &ictx);
    ictx->istream = NULL;

    ictx->params[0] = spa_format_audio_raw_build(&ictx->b, SPA_PARAM_EnumFormat,
                                                 &SPA_AUDIO_INFO_RAW_INIT(
                                                         .format = SPA_AUDIO_FORMAT_F32P,
                                                         .channels = params->channels,
                                                         .rate = params->sampleRate));
    pw_stream_connect(ictx->stream,
                      PW_DIRECTION_OUTPUT,
                      interface,
                      PW_STREAM_FLAG_AUTOCONNECT |
                          PW_STREAM_FLAG_MAP_BUFFERS |
                          PW_STREAM_FLAG_RT_PROCESS,
                      ictx->params, 1);

    pthread_create(&ictx->sampleThread, NULL, runMainLoop, ictx);
    return 0;
}
#endif // BAC_NO_PW
#endif // __MINGW64__