//
// Created by bryan on 4/15/22.
//

/*
 * Begin license text.
 *
 * Copyright 2022 Bryan Sharpe
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * End license text.
 */
#ifndef __MINGW64__
#ifndef BAC_NO_PW
#ifndef BAC_BAC_PW_H
#define BAC_BAC_PW_H
#include <stdint.h>
#include "BAC.h"
/**
 * @return 0 always as of 1.0.0, potentially error codes in the future
 */
int bac_pw_start(uint32_t interface, BAC_t *ctx, BACParams_t *params);
#endif // BAC_BAC_PW_H
#endif
#endif