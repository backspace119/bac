
#ifndef BAC_PA_H
#define BAC_PA_H
#include "BAC.h"

int bac_pa_start(BAC_t *ctx);
void bac_pa_stop(BAC_t *ctx);

#endif