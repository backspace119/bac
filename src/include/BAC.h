/**
 *
 * Begin license text.
 *
 * Copyright 2022 Bryan Sharpe
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * End license text.
 *
 * Main Header file for Bonk'd Audio Capture (BAC) tool. A couple notes on implementation details:
 *
 * BAC is designed with speed and efficiency in mind. By default I do very little error checking and it is up to you
 * the programmer to ensure you don't break anything. I have hidden away many internal details outside of the interface
 * so you don't have to worry about them (and can't break them (although I invite you to poke around the source and get
 * a feel for how things are done, the point I'm making is I've tried to make this easy to use, not that I want to hide
 * the source away from the user)) but there are obvious things you can do that will cause
 * this library to break (such as freeing in use resources rather than relying on bac_stop to clean up those resources)
 *
 * None of the resources created by this library are expected to be cleaned up by you, BAC will handle it
 *
 * By default I've found that ALSA likes to connect to a microphone when you set up for capture, as of version 1.0.0 I have
 * not found a good way around this, and so by default it will capture from the mic. I have found a manual work around
 * (if you are using PipeWire) by using a tool such as Helvium to reroute the connections to your system's output audio
 * [monitor] outputs. The PipeWire implementation does not have this problem, it uses some cheeky bastard level shit to
 * get you connected to the system default audio output (see notes in the BAC_PW.c file for details).
 *
 * Samples are (by default) stored in flat buffers without channel interleaving, this is to make doing things like fourier transformations
 * with a tool like FFTW3 easy, we can do simple memcpys to get the data into the tool rather than having to iterate to fill
 * arrays. This means that, if you had a sample size of say, 8 (way too small, but for the sake of my keyboard and typing
 * this out I'll use this example) and 2 channels (stereo LR) the data will be laid out like this:
 *
 * [L,L,L,L,L,L,L,L,R,R,R,R,R,R,R,R]
 *
 * The buffers are accessed from time to time through pointers into the buffer, an optimization may be made in the future
 * to avoid doing this, but for the moment it is done this way to allow extensibility of passing your own external
 * buffer that we will write directly to
 *
 * I am not certain of the internal details of ALSA but I am guessing they do a memcpy per channel, in the PipeWire
 * implementation I am doing a memcpy per channel, because they've handed me the individual buffers, where alsa takes a
 * void**. This means performance will slightly decrease as channels go up on the library side, although the library is
 * very lightweight and likely whatever you're doing with the data will take much longer anyway.
 *
 * This library, as of version 1.0.0, does not require anything except std C libraries (I believe C99 should do), ALSA
 * (libasound), PipeWire (libpipewire-0.3) and pthreads (libpthread) to compile the library itself. The test program
 * additionally requires SDL2 (libSDL2) and FFTW3 (libfftw3f (note it is the 32 bit float version)) to compile. In the
 * future I plan to allow compiling only the ALSA or PipeWire versions, to allow for use on more systems, a windows-compatible
 * library is not, and probably never will be, planned.
 *
 * Documentation can be found in relevant header files, although BAC_PW and BAC_ALSA are not documented since the functions
 * in this file are essentially wrappers for those (there is a note in each about the start return types though).
 *
 * ===TESTING===
 * Test Setup: Fedora 36 x86_64 AMD Threadripper 1950x CPU, 64 GB RAM, Radeon 6900 XT GPU
 *
 * Test Program: BAC_test, source included in this repository, it is an audio visualizer application that draws frequency
 * a spectrogram, and power information
 *
 * Test settings:
 * Sample Rate: 192khz
 * Sample Size: 4096
 *
 * Average Memory consumption of test program: 125 MB (majority of this is SDL)
 * Average CPU usage (from top) of test program: ~30% (spread across ~3 threads, render thread is heaviest pushing probably 20% on a single core)
 * Average Frame Draw Time at 4k resolution: ~11ms, CPU limited
 *
 * Other sample rates and sizes have been tested as well, 48k rate with 1k size being the default, since *most* systems
 * should probably support this, and it's a decent sample size for that rate, (equates to ~21.3ms of audio data)
 *
 *
 * Implementation notes and Specifics:
 * WARNING!!! WHEN USING PIPEWIRE WE FORCE QUANTUM AND RATE TO WHAT YOU REQUESTED, THIS IS TO PREVENT RECEIVING A LARGER
 * BUFFER THAN YOU REQUESTED FOR YOUR SAMPLE SIZE
 *
 * When using ALSA your params sampleRate may change after initialization, we pass it into ALSA when we request rate, and
 * ALSA writes the rate it actually gave you back to it, you should check this value after init and adjust your math
 * accordingly, during testing the rate never differed from what was requested, but forgo the check at your own risk.
 *
 */
#ifndef BAC_BAC_H
#define BAC_BAC_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <assert.h>
#ifndef __MINGW64__
#include <pthread.h>
#else
#include </usr/x86_64-w64-mingw32/sys-root/mingw/include/pthread.h> // to future builders of this project, fuck you
#endif
#ifndef BAC_DEBUG
#define BAC_DEBUG(ctx, ...) bac_debug(ctx, __VA_ARGS__)
#endif // BAC_DEBUG
#ifndef BAC_ERROR
#define BAC_ERROR(...) \
    fprintf(stderr, __VA_ARGS__);
#endif // BAC_ERROR
       // clang-format off
#define BAC_VERSION_MAJOR           1
#define BAC_VERSION_MINOR           3
#define BAC_VERSION_PATCH           2

#define BAC_DEFAULT_INTERFACE_ALSA  "default"
#ifndef BAC_NO_PW
#define BAC_DEFAULT_INTERFACE_PW    0xFFFFFFFF //this is stolen from a PW header so we don't have to include it in this one
#endif
#define BAC_CTX_NOT_READY           1
#define BAC_READ_SUCCESSFUL         0
#define BAC_CORRUPT                 2
#define BAC_SAMPLE_RATE_384K        384000
#define BAC_SAMPLE_RATE_192K        192000
#define BAC_SAMPLE_RATE_96K         96000
#define BAC_SAMPLE_RATE_48K         48000
#define BAC_SAMPLE_RATE_176_4K      176400
#define BAC_SAMPLE_RATE_88_2k       88200
#define BAC_SAMPLE_RATE_44_1K       44100
#define BAC_DEFAULT_SAMPLE_RATE     BAC_SAMPLE_RATE_48K
#define BAC_DEFAULT_CHANNELS        2
#define BAC_FLAGS_EXTERNAL_BUFFER   0x01    /*  use this flag and provide pointers in externalBuffer in the params with at least
                                             *  channels amount of pointers and each points to sizeof(float) * sampleSize
                                             *  of valid memory. WE WILL WRITE TO THIS BUFFER WITHOUT WARNING! It is necessary
                                             *  to do so in order to prevent buffer overflow in ALSA or PipeWire (not strictly
                                             *  necessary for either, since ALSA uses a ringbuffer and PipeWire sends individual
                                             *  buffers that get recycled, but this is a design point of BAC to allow some
                                             *  guarantee that you will get every frame
                                             */
#define BAC_FLAGS_CALL_BEFORE_WRITE 0x02    /* use this flag to indicate you want to use a callback before we write to
                                             * the buffer, we will pass the user data as the first, the pointer to sample pointers as the second argument
                                             * and the channels and sample size as the third and forth arguments
                                             * this is likely only useful when used in conjunction with BAC_FLAGS_EXTERNAL_BUFFER
                                             * DO NOT BLOCK ON THIS CALLBACK, IT MAY BE RUN IN A REAL TIME THREAD!!!!!
                                             */
#define BAC_FLAGS_CALL_AFTER_WRITE  0x04    /* use this flag to indicate you want a finalization callback called after we have finished writing to the buffer
                                             * it will receive the same arguments as a call before writing, and has the same warnings along with it
                                             * 
                                             */
/**
 * SAMPLE SIZES
 * a larger sample size will result in a longer time period of audio signal given to you for each sample, this is a
 * balance between giving yourself enough time to do what you need to and keeping latency low, in my testing I've found
 * 4096 to be a decent sample size for my use cases, using the test hardware listed in the notes above, for the use case
 * of the test program included with this repository. You may need to experiment on your own to find what works best,
 * my recommendation is to start at larger values and move your way down, as latency will decrease as you move downward,
 * requiring you to sit on the buffer more often
 */
#define BAC_SAMPLE_SIZE_8K          8192
#define BAC_SAMPLE_SIZE_4K          4096
#define BAC_SAMPLE_SIZE_2K          2048
#define BAC_SAMPLE_SIZE_1K          1024
#define BAC_SAMPLE_SIZE_512         512
#define BAC_SAMPLE_SIZE_256         256
#define BAC_SAMPLE_SIZE_128         128
#define BAC_DEFAULT_SAMPLE_SIZE     BAC_SAMPLE_SIZE_1K
    // clang-format on
    typedef uint32_t BAC_Flags_t;
    /**
     * which audio interface to use, ALSA (Advanced Linux Sound Architecture) or PipeWire, ALSA is lower level (it exists on
     * most linux systems and is the audio driver for the system) but PipeWire is newer and a bit more extensible (and only
     * available on newer distributions that have already adopted it, i.e. Fedora) see the notes above about the caveat of
     * ALSA
     */
    typedef enum
    {
#ifndef __MINGW64__
        ALSA = 0x0,
#ifndef BAC_NO_PW
        PIPEWIRE = 0x1,
#endif
#endif
        PORTAUDIO = 0x2,
    } BAC_INTERFACE_e;

    typedef void *BAC_Internal_t;
    typedef void (*bac_bufferPrep)(void *userdata, float **externalBuffer, uint32_t channels, uint32_t sampleSize);
    typedef void (*bac_finalize)(void *userdata, float **externalBuffer, uint32_t channels, uint32_t sampleSize);
    typedef int (*bac_debug_cb)(const char *fmt, ...);
    /**
     * parameters to initialize BAC with, it is safe to destroy these after calling bac_start, we copy the values into
     * our own memory, initialize all values up to and including flags, values after are optional and dependent on the flags you use
     * DEFAULT_PW_PARAMS and DEFAULT_ALSA_PARAMS are provided for convenience for sane PW and ALSA defaults, respectively,
     * but it is good practice to initialize them to sane defaults
     *
     * You only need to fill in alsaInterface or pwInterface depending on if you are using ALSA or PipeWire, respectively
     * BAC_DEFAULT_INTERFACE_ALSA and BAC_DEFAULT_INTERFACE_PW have been provided for convenience, the default ALSA interface
     * is "default" the default PW interface is PW_ANY_ID (0xFFFFFFFF)
     */
    typedef struct
    {
        uint32_t sampleSize;
        uint32_t sampleRate;
        uint32_t channels;
        BAC_INTERFACE_e interface;
        char *alsaInterface;
#ifndef BAC_NO_PW
        uint32_t pwInterface;
#endif
        BAC_Flags_t flags;
        float **externalBuffer;
        bac_bufferPrep bpcb;
        void *userData;
        bac_finalize fcb;
        bac_debug_cb dcb;
    } BACParams_t;

#define DEFAULT_PW_PARAMS                        \
    {                                            \
        .sampleSize = BAC_DEFAULT_SAMPLE_SIZE,   \
        .channels = BAC_DEFAULT_CHANNELS,        \
        .sampleRate = BAC_DEFAULT_SAMPLE_RATE,   \
        .interface = PIPEWIRE,                   \
        .alsaInterface = NULL,                   \
        .pwInterface = BAC_DEFAULT_INTERFACE_PW, \
        .flags = 0,                              \
        .bpcb = NULL,                            \
        .userData = NULL,                        \
        .fcb = NULL,                             \
    }

#define DEFAULT_ALSA_PARAMS                          \
    {                                                \
        .sampleSize = BAC_DEFAULT_SAMPLE_SIZE,       \
        .channels = BAC_DEFAULT_CHANNELS,            \
        .sampleRate = BAC_DEFAULT_SAMPLE_RATE,       \
        .interface = ALSA,                           \
        .alsaInterface = BAC_DEFAULT_INTERFACE_ALSA, \
        .pwInterface = 0,                            \
        .flags = 0,                                  \
        .bpcb = NULL,                                \
        .userData = NULL,                            \
        .fcb = NULL,                                 \
    }

    typedef struct
    {
        uint32_t major;
        uint32_t minor;
        uint32_t patch;
    } BAC_Version_t;

    /**
     * main BAC context, keep this around to use the library, make sure to call bac_stop(ctx*) to clean things up when you are
     * done
     */
    typedef struct
    {
        pthread_cond_t readyCond;
        volatile uint8_t ready : 1;
        volatile uint8_t done : 1;
        volatile uint8_t run : 1;
        pthread_mutex_t readMutex;
        pthread_mutex_t writeMutex;
        float **sample;
        BACParams_t params;
        BAC_Internal_t ictx;
    } BAC_t;

    /**
     * gets the major version of the library binary you have, you should check this against the header you built against
     * the guarantee here is a major version change WILL break the API, and so you SHOULD NOT continue running
     * @return major version
     */
    uint32_t getVersionMajor();

    /**
     * gets the minor version of the library binary you have, you should check this against the header you built against
     * the guarantee here is a minor version change WILL change internal behavior in a significant way, while maintaining
     * the API, you MAY be able to continue running
     * @return
     */
    uint32_t getVersionMinor();

    /**
     * gets the patch version of the library binary you have, you should check this against the header you built against
     * the guarantee here is a patch version WILL change internal behavior in an insignificant way, while maintaining the
     * API, you can MOST LIKELY continue running
     * @return
     */
    uint32_t getVersionPatch();

    /**
     * Gets the version as a struct instead of an individual uint, for eas of use
     * @return
     */
    BAC_Version_t getVersion();

    int bac_debug(BAC_t *ctx, const char *fmt, ...);

    /**
     * checks if a flag is set, returns flag value if true 0 if false
     * @param flag flag to check
     * @param params params to check if flag is set
     * @return flag value or 0
     */
    uint32_t bac_checkFlag(BAC_Flags_t flag, BACParams_t *params);

    /**
     *  starts reading audio and pushing it to the sample buffer, this creates a thread to manage the process, and returns immediately,
     *  use bac_getSampleCombined or bac_getSampleCombinedN (non blocking) to retrieve a sample into your own buffer,
     *  reading directly from sample may provide corrupt data unless you first lock the writeMutex provided by the context struct
     *  while the writeMutex is locked frames are dropped from the audio stream, this function will modify the value of the
     *  sampleRate in the params passed in if we couldn't get the requested rate
     * @param ctx empty BAC context struct, values will be initalized by this function, including the sample buffer
     * @param params parameters to initialize BAC with, it is safe to destroy these after calling this function, we copy out the values
     * @return 0 on succes, error from interface on failure, or BAC_CORRUPT (2) if bad interface is provided
     */
    int bac_start(BAC_t **ctx, BACParams_t *params);

    /**
     * stops BAC, waits until thread is done and cleans up ALSA bindings and BAC resources
     * @param ctx BAC context
     */
    void bac_stop(BAC_t *ctx);

    /**
     * if you made a dependency loop (like me) this is your way out
     * @param ctx BAC_t context to update userdata that will be passed to the buffer prep callback
     * @param userData value to set
     */
    void bac_updateUserData(BAC_t *ctx, void *userData);

    /**
     * Hint: getting combined samples uses a single memcpy, getting samples into pointers to pointers uses channels memcpys
     * blocks until a sample is available
     * @param ctx BAC context
     * @param sampleMem pointer where we will copy the data to, should be sizeof(float) * sampleSize * channels large
     *          the layout of the buffer is sampleSize floats CH0, sampleSize floats CH1, sampleSize floats CH2, etc, generally,
     *          CH0 is L and CH1 is R, beyond this consult other manuals
     */
    void bac_getSampleCombined(BAC_t *ctx, float *sampleMem);

    /**
     * Hint: getting combined samples uses a single memcpy, getting samples into pointers to pointers uses channels memcpys
     * returns BAC_CTX_NOT_READY if we don't have a sample right now, BAC_READ_SUCCESSFUL if we read into the buffer successfully
     * non blocking, except if the writeMutex is locked by the sampler thread, at which point we will wait until it unlocks to lock it ourselves
     * @param ctx BAC context
     * @param sampleMem pointer to where we will copy the data to, should be sizeof(float) * sampleSize * channels large
     *          the layout of the buffer is sampleSize floats CH0, sampleSize floats CH1, sampleSize floats CH2, etc, generally,
     *          CH0 is L and CH1 is R, beyond this consult other manuals
     * @return BAC_CTX_NOT_READY (1) on fail to read into buffer, BAC_READ_SUCCESSFUL (0) on success BAC_CORRUPT (2) on failure
     */
    uint8_t bac_getSampleCombinedN(BAC_t *ctx, float *sampleMem);

    /**
     * Hint: getting combined samples uses a single memcpy, getting samples into pointers to pointers uses channels memcpys
     * blocks until a sample is available
     * @param ctx BAC context
     * @param sampleMem a pointer to pointers with channels number of pointers that each point to sampleSize * sizeof(float) valid memory
     */
    void bac_getSample(BAC_t *ctx, float **sampleMem);

    /**
     * Hint: getting combined samples uses a single memcpy, getting samples into pointers to pointers uses channels memcpys
     * returns BAC_CTX_NOT_READY if we don't have a sample right now, BAC_READ_SUCCESSFUL if we read into the buffer successfully
     * non blocking, except if the writeMutex is locked by the sampler thread, at which point we will wait until it unlocks to lock it ourselves
     * @param ctx BAC context
     * @param sampleMem  a pointer to pointers with channels number of pointers that each point to sampleSize * sizeof(float) valid memory
     * @return BAC_CTX_NOT_READY (1) on fail to read into buffer, BAC_READ_SUCCESSFUL (0) on success BAC_CORRUPT (2) on failure
     */
    uint8_t bac_getSampleN(BAC_t *ctx, float **sampleMem);

#ifdef __cplusplus
}
#endif
#endif // BAC_BAC_H
