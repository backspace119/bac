//
// Created by bryan on 4/16/22.
//
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include "include/BAC.h"
#include "include/BAC_ALSA.h"
#include "include/BAC_PW.h"
#include "include/BAC_PA.h"
#include <stdarg.h>

const BAC_Version_t BAC_VERSION = {
    .major = BAC_VERSION_MAJOR,
    .minor = BAC_VERSION_MINOR,
    .patch = BAC_VERSION_PATCH};

uint32_t getVersionMajor()
{
    return BAC_VERSION_MAJOR;
}
uint32_t getVersionMinor()
{
    return BAC_VERSION_MINOR;
}
uint32_t getVersionPatch()
{
    return BAC_VERSION_PATCH;
}

uint32_t bac_checkFlag(BAC_Flags_t flag, BACParams_t *params)
{
    return (flag & params->flags);
}

BAC_Version_t getVersion()
{
    return BAC_VERSION;
}

int bac_debug(BAC_t *ctx, const char *fmt, ...)
{
    if(!ctx->params.dcb) return 0;
    va_list list;
    va_start(list,fmt);
    int ret = ctx->params.dcb(fmt, list);
    va_end(list);
    return ret;
}

int bac_start(BAC_t **ctx, BACParams_t *params)
{
    *ctx = malloc(sizeof(BAC_t));
    pthread_cond_init(&(*ctx)->readyCond, NULL);
    pthread_mutex_init(&(*ctx)->writeMutex, NULL);
    pthread_mutex_init(&(*ctx)->readMutex, NULL);
    (*ctx)->done = 0;
    (*ctx)->run = 1;
    (*ctx)->ready = 0;
    (*ctx)->params = *params;
    if (!bac_checkFlag(BAC_FLAGS_EXTERNAL_BUFFER, params))
    {
        (*ctx)->sample = malloc(sizeof(float *) * params->channels);
        (*ctx)->sample[0] = malloc(sizeof(float) * params->sampleSize * params->channels);
        for (int i = 1; i < params->channels; i++)
        {
            (*ctx)->sample[i] = &(*ctx)->sample[0][i * params->sampleSize];
        }
    }
    else
        (*ctx)->sample = params->externalBuffer;
    switch (params->interface)
    {
#ifndef __MINGW64__
    case ALSA:
        return bac_alsa_start(params->alsaInterface, *ctx, params);
#ifndef BAC_NO_PW
    case PIPEWIRE:
        return bac_pw_start(params->pwInterface, *ctx, params);

#endif
#endif
    case PORTAUDIO:
        return bac_pa_start((*ctx));
    default:
    {
        BAC_DEBUG(*ctx,"unsupported interface selected: %i, please use ALSA or PIPEWIRE from the BAC_INTERFACE_e\n", params->interface);
        return -1;
    }
    }
}

void bac_updateUserData(BAC_t *ctx, void *userData)
{
    ctx->params.userData = userData;
}

void bac_stop(BAC_t *ctx)
{

    ctx->run = 0;
    while (!ctx->done && !(ctx->params.interface == PORTAUDIO)) // pa has it's own threads and management, and doesn't call the callback unless sound is playing, just skip the wait and head straight for nuking it
    {
        usleep(1000);
    }
    if (ctx->params.interface == PORTAUDIO)
    {
        bac_pa_stop(ctx);
    }
    if (!bac_checkFlag(BAC_FLAGS_EXTERNAL_BUFFER, &ctx->params))
    {
        free(ctx->sample[0]);
        free(ctx->sample);
    }
    pthread_cond_destroy(&ctx->readyCond);
    pthread_mutex_destroy(&ctx->readMutex);
    pthread_mutex_destroy(&ctx->writeMutex);
    free(ctx);
}

void bac_getSampleCombined(BAC_t *ctx, float *sampleMem)
{
    pthread_mutex_lock(&ctx->readMutex);
    pthread_cond_wait(&ctx->readyCond, &ctx->readMutex);
    pthread_mutex_lock(&ctx->writeMutex);
    memcpy(sampleMem, ctx->sample[0], sizeof(float) * ctx->params.sampleSize * ctx->params.channels);
    ctx->ready = 0;
    pthread_mutex_unlock(&ctx->writeMutex);
    pthread_mutex_unlock(&ctx->readMutex);
}

uint8_t bac_getSampleCombinedN(BAC_t *ctx, float *sampleMem)
{
    if (ctx->ready)
    {
        pthread_mutex_lock(&ctx->writeMutex);
        memcpy(sampleMem, ctx->sample[0], sizeof(float) * ctx->params.sampleSize * ctx->params.channels);
        ctx->ready = 0;
        pthread_mutex_unlock(&ctx->writeMutex);
        return BAC_READ_SUCCESSFUL;
    }
    else
        return BAC_CTX_NOT_READY;
}

void bac_getSample(BAC_t *ctx, float **sampleMem)
{
    pthread_mutex_lock(&ctx->readMutex);
    pthread_cond_wait(&ctx->readyCond, &ctx->readMutex);
    pthread_mutex_lock(&ctx->writeMutex);
    for (int i = 0; i < ctx->params.channels; i++)
        memcpy(sampleMem[i], ctx->sample[i], sizeof(float) * ctx->params.sampleSize);
    ctx->ready = 0;
    pthread_mutex_unlock(&ctx->writeMutex);
    pthread_mutex_unlock(&ctx->readMutex);
}

uint8_t bac_getSampleN(BAC_t *ctx, float **sampleMem)
{
    if (ctx->ready)
    {
        pthread_mutex_lock(&ctx->writeMutex);
        for (int i = 0; i < ctx->params.channels; i++)
            memcpy(sampleMem[i], ctx->sample[i], sizeof(float) * ctx->params.sampleSize * ctx->params.channels);
        ctx->ready = 0;
        pthread_mutex_unlock(&ctx->writeMutex);
        return BAC_READ_SUCCESSFUL;
    }
    else
        return BAC_CTX_NOT_READY;
}
