
#include "BAC_PA.h"
#include </usr/include/portaudio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct
{
   PaStream *stream;
   BAC_t *ctx;
   uint64_t framesCollected;
} BAC_PA_t;

static int paCallback(
    const void *input, void *output,
    unsigned long frameCount,
    const PaStreamCallbackTimeInfo *timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData)
{

   BAC_PA_t *ictx = (BAC_PA_t *)userData;
   BAC_t *ctx = ictx->ctx;
   if (!ctx->run && !ctx->done)
   {
      ctx->done = 1;
      printf("shutting down pa callback\n");
      return paComplete;
   }
   if (!pthread_mutex_trylock(&ictx->ctx->writeMutex))
   {
      uint64_t oFramesCollected = ictx->framesCollected;
      ictx->framesCollected += frameCount;
      if (ictx->framesCollected >= ictx->ctx->params.sampleSize)
      {
         if (bac_checkFlag(BAC_FLAGS_CALL_BEFORE_WRITE, &ctx->params))
         {
            ctx->params.bpcb(ctx->params.userData, ctx->sample, ctx->params.channels, ctx->params.sampleSize);
         }
      }
      if (frameCount > (ctx->params.sampleSize - oFramesCollected))
         frameCount = (ctx->params.sampleSize - oFramesCollected); // dropping frames for now, figure this out
      for (int i = 0; i < ctx->params.channels; i++)
      {
         memcpy(&ctx->sample[i][oFramesCollected], ((float **)input)[i], frameCount * sizeof(float));
      }
      if (ictx->framesCollected >= ictx->ctx->params.sampleSize)
      {
         if (bac_checkFlag(BAC_FLAGS_CALL_AFTER_WRITE, &ctx->params))
         {
            ctx->params.fcb(ctx->params.userData, ctx->sample, ctx->params.channels, ctx->params.sampleSize);
         }
         pthread_cond_broadcast(&ctx->readyCond);
         ictx->framesCollected = 0;
      }
      pthread_mutex_unlock(&ictx->ctx->writeMutex);
   }
   return paContinue;
}

void bac_pa_stop(BAC_t *ctx)
{
   Pa_Terminate();
}

int bac_pa_start(BAC_t *ctx)
{
   int err;
   if ((err = Pa_Initialize()))
   {
      BAC_ERROR("error initializing port audio: %s\n", Pa_GetErrorText(err));
      return err;
   }
   BAC_DEBUG(ctx, "version: %x\n", Pa_GetVersion());

   BAC_DEBUG(ctx, "apis: %i\n", Pa_GetHostApiCount());

   PaDeviceIndex index = Pa_GetDefaultOutputDevice();
   const PaDeviceInfo *info = Pa_GetDeviceInfo(index);
   char name[strlen(info->name) + strlen(" [Loopback]") + 1];
   memset(name, 0, sizeof(name));
   memcpy(name, info->name, strlen(info->name));
   sprintf(&name[strlen(info->name)], " [Loopback]");
   BAC_DEBUG(ctx, "loopback name: %s\n", name);

   BAC_DEBUG(ctx, "all input devices:\n");
   for (int i = 0; i < Pa_GetDeviceCount(); i++)
   {
      const PaDeviceInfo *info = Pa_GetDeviceInfo(i);
      if (info->maxInputChannels == 0)
         continue;
      if (!strcmp(name, info->name))
      {
         BAC_DEBUG(ctx, "selecting device: %i\n", i);
         index = i;
      }
      BAC_DEBUG(ctx, "device: %i %s\n", i, info->name);
      BAC_DEBUG(ctx, "    output channels: %i\n", info->maxOutputChannels);
      BAC_DEBUG(ctx, "    input channels: %i\n", info->maxInputChannels);
   }

   BAC_DEBUG(ctx, "default device index: %i\n", Pa_GetDefaultOutputDevice());
   BAC_DEBUG(ctx, "using index: %i\n", index);
   PaStreamParameters inputParams = {0};
   inputParams.channelCount = ctx->params.channels;
   inputParams.device = index;
   inputParams.hostApiSpecificStreamInfo = NULL;
   inputParams.sampleFormat = paFloat32 | paNonInterleaved;
   BAC_PA_t *ictx = malloc(sizeof(BAC_PA_t));
   ictx->ctx = ctx;
   ctx->ictx = ictx;
   if (!Pa_IsFormatSupported(&inputParams, NULL, ctx->params.sampleRate))
   {
      BAC_DEBUG(ctx, "Requested format not supported, assuming bad sample rate and setting accordingly...\n");
      ctx->params.sampleRate = Pa_GetDeviceInfo(index)->defaultSampleRate; // assume problem is sample rate, probably need to deal with format as well
   }
   // do not specify number of frames for maximum compatibility, handle buffering this properly in the callback
   err = Pa_OpenStream(&ictx->stream, &inputParams, NULL, ctx->params.sampleRate, paFramesPerBufferUnspecified, paNoFlag, paCallback, ictx);
   if (err)
   {
      BAC_ERROR("Error opening stream: %s\n", Pa_GetErrorText(err));
      return err;
   }
   Pa_StartStream(ictx->stream);
   return err;
}
