SHELL = /bin/sh
CC			=gcc
CXX			=g++
TCMALLOC 	= -ltcmalloc
SRCDIR		= ./src
BUILDDIR	= build
BINDIR 		= bin
DEBUG_FLAGS	= -g -fsanitize=undefined -Wall
CFLAGS		= -fPIC -I$(SRCDIR)/include -I/usr/include/pipewire-0.3/ -I/usr/include/spa-0.2/ -O3 -pthread -Wall -g
CXXFLAGS	= -std=c++14 $(CFLAGS)

SRCXXEXT    = cpp
SRCEXT 		= c
DEPEXT      = d
OBJEXT      = o

SOURCES 	= $(shell find $(SRCDIR) -type f -regex "\.\/.+.c[pp]*")
HEADERS 	= $(shell find $(SRCDIR)/include -type f -regex "\.\/.+.h")
TOBJECTS 	= $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))
OBJECTS 	= $(TOBJECTS:.$(SRCXXEXT)=.$(OBJEXT))

LIBWIN 		= -lpthread -L./build_libs/ -lportaudio
LIB_NO_PW	= -lasound -lpthread -lportaudio
LIB			= -lasound -lpthread -lpipewire-0.3 -lportaudio
TESTLIB_NOPW= -L./bin -lSDL2 -lm -lfftw3f -lbacnopw
TESTLIB		= -L./bin -lSDL2 -lm -lfftw3f -lbac
TARGET_NOPW = $(BINDIR)/libbacnopw.so
TARGET 		= $(BINDIR)/libbac.so
TARGETWIN	= $(BINDIR)/libbac.dll
LDFLAGS		= -L./build_libs/libportaudio.dll.a -shared -Wl,-rpath=/usr/local/lib -fPIC
TESTLD		= -Wl,-rpath=/usr/local/lib
RM			= rm -f
MKDIR		= mkdir
TESTSRC		= ./test/BAC_test.c ./test/BACSDL.c
TESTBIN		= $(BINDIR)/bac

all 					: clean linux-test
linux 				: directories $(TARGET)
linux-no-pw 		: CFLAGS += -D BAC_NO_PW
linux-no-pw			: LIB = $(LIB_NO_PW)
linux-no-pw			: TESTLIB = $(TESTLIB_NOPW)
linux-no-pw			: TARGET = $(TARGET_NOPW)
linux-no-pw			: directories $(TARGET_NOPW)
linux-test 			: linux install test
linux-test-no-pw	: CFLAGS += -D BAC_NO_PW
linux-test-no-pw	: LIB = $(LIB_NO_PW)
linux-test-no-pw	: TESTLIB = $(TESTLIB_NOPW)
linux-test-no-pw	: TARGET = $(TARGET_NOPW)
linux-test-no-pw	: linux-no-pw install test
objects				: $(OBJECTS)

windows				: CC = x86_64-w64-mingw32-gcc
windows				: TARGET = $(TARGETWIN)
windows				: LIB = $(LIBWIN)
windows				: directories $(TARGETWIN)

directories:
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(BINDIR)

$(BUILDDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BUILDDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCXXEXT)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(TARGET_NOPW): $(OBJECTS)
	$(CC) ${LDFLAGS} -o $@ $^ $(LIB)

$(TARGET): $(OBJECTS)
	$(CC) ${LDFLAGS} -o $@ $^ $(LIB)
	# -${RM} ${OBJECTS}

$(TARGETWIN): $(OBJECTS)
	$(CC) ${LDFLAGS} -o $@ $^ $(LIB)
.PHONY: test
test: $(TESTSRC)
	$(CC)  $(CFLAGS) ${TESTLD} -o $(TESTBIN) $^ $(LIB) $(TESTLIB)

install:
	sudo cp $(TARGET) /usr/local/lib

install-headers: $(HEADERS)
	echo $^
	sudo cp $^ /usr/include

.PHONY: clean
clean:
	-${RM} ${TARGET} ${OBJECTS} ${SHADER_BIN} $(BINDIR)/bac ${TARGETWIN} ${TARGET_NOPW}